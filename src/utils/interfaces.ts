export interface Settings {
    ALLOW_USERS: string;
    BITFINEX_BALANCE: number;
    CLIENT_ID: number;
    HOMEPAGE: string;
    MARGIN_CZK: number;
    MARGIN_BTC: number;
    ORIGIN_BTC: number;
    ORIGIN_CZK: number;
    PRIVATE_KEY_CM: string;
    PUBLIC_KEY_CM: string;
    PRIVATE_KEY_BFX: string;
    PUBLIC_KEY_BFX: string;
}

export interface CurrencyStructure {
    rate: number;
    balance: number;
    avb: number;
    result: number;
}

export interface Balance {
    [index: number]: CurrencyStructure;
}

export interface DatabaseRecord {
    key: number;
    data: Balance;
    date: number;
    fullBalance: number;
    hodl: number;
    dif: number;
    profit: number;
}

export declare type CoinMateBalanceType = 'BTC' | 'EUR' | 'CZK' | 'BCH' | 'XRP' | 'ETH' | 'LTC' | 'DASH' | 'DAI';

export interface CoinMateBalance {
    BTC: {
        currency: 'string';
        balance: number;
        reserved: number;
        available: number;
    };
    EUR: {
        currency: 'string';
        balance: number;
        reserved: number;
        available: number;
    };
    CZK: {
        currency: 'string';
        balance: number;
        reserved: number;
        available: number;
    };
    BCH: {
        currency: 'string';
        balance: number;
        reserved: number;
        available: number;
    };
    XRP: {
        currency: 'string';
        balance: number;
        reserved: number;
        available: number;
    };
    ETH: {
        currency: 'string';
        balance: number;
        reserved: number;
        available: number;
    };
    LTC: {
        currency: 'string';
        balance: number;
        reserved: number;
        available: number;
    };
    DASH: {
        currency: 'string';
        balance: number;
        reserved: number;
        available: number;
    };
    DAI: {
        currency: 'string';
        balance: number;
        reserved: number;
        available: number;
    };
}

export interface CoinMateTicker {
    last: number;
    high: number;
    low: number;
    amount: number;
    bid: number;
    ask: number;
    change: number;
    open: number;
    timestamp: number;
}

export interface ExchangeRatesApi {
    EUR_USD: number;
    EUR_CZK: number;
}

export interface TradeRow {
    transactionId: number;
    createdTimestamp: number;
    currencyPair: string;
    type: string;
    orderType: string;
    orderId: number;
    amount: number;
    price: number;
    fee: number;
    feeType: string;
}

export interface MovementColumn {
    title: string;
    key: string;
    width?: number;
    render: (x: DatabaseRecord) => void;
}

export interface State {
    authUser: firebase.default.User | null;
    settings: Settings;
    database: DatabaseRecord[];
}

export interface DefaultProps {
    authUser: firebase.default.User | null;
    settings: Settings;
    database: DatabaseRecord[];

    updateAuthUser: (authUser: firebase.default.User | null) => void;
    updateSettings: (settings: Settings) => void;
    updateDatabase: (database: DatabaseRecord[]) => void;
}
