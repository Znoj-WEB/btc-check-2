### **Description**

Balance from CoinMate count full in CZK.  
Data are save in Firebase Real-Time db.  
There is a table with last transactions with separate button for getting these data from CoinMate API.

file .env has to contain:

REACT_APP_FIREBASE_API_KEY = ""  
REACT_APP_FIREBASE_AUTH_DOMAIN = ""  
REACT_APP_FIREBASE_DATABASE_URL = ""  
REACT_APP_FIREBASE_PROJECT_ID = ""  
REACT_APP_FIREBASE_STORAGE_BUCKET = ""  
REACT_APP_FIREBASE_MESSAGING_SENDER_ID = ""  
REACT_APP_FIREBASE_APP_ID = ""

There is dollars input for setting amout of Dollars outside CoinMate.  
BTC mount is increment by 0.1 due to my personal reasons.  
MARGIN in settings is for proper counting, if I want to track some money outside statistic (add and not change ORIGINs).  
ORIGIN_CZK and ORIGIN_BTC are set for statistics, because I am comparing HODL and TRADING.
db access rules are managed by database.rules.json file

---

### **Link**

served with Firebase:  
`not public`

---

### **Technology**

TypeScript, Ant Design, React, Hooks, Redux, Firebase, GitLab CI/CD

---

### **Year**

2020

---

### **Screenshots**

![](./README/main.png)

![](./README/m6FullTable.png)

![](./README/m1Login.png)

![](./README/m2LoginFailed.png)

![](./README/m3Top.png)

![](./README/m4Bottom.png)

![](./README/m5Message.png)
